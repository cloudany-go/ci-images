#!/bin/bash

# expose necessary environment & apply all .k8s.yml profiles

set -e

function info() { printf "\033[0;92m%b\033[0m\n" "${*}"; }
function error() { printf "\033[0;91m%b\033[0m\n" "${*}"; }

# obtains kube config
[ -d "${HOME}/.kube" ] || mkdir "${HOME}/.kube"
echo $KUBECONFIG_DATA | base64 -d > "${HOME}/.kube/config"

kubectl config set-context --current --namespace=${K8S_NAMESPACE}
kubectl cluster-info

# make sure we export enough data
export IMAGE_PULL_SECRET=${IMAGE_PULL_SECRET:-gitlab-regcred}
export MIN_REPLICAS=${MIN_REPLICAS:-1}
export MAX_REPLICAS=${MAX_REPLICAS:-4}
export CPU_AVERAGE_UTILIZATION=${CPU_AVERAGE_UTILIZATION:-70}
export REQUESTS_CPU=${REQUESTS_CPU:-200m}
export REQUESTS_MEMORY=${REQUESTS_MEMORY:-64Mi}
export LIMITS_CPU=${LIMITS_CPU:-500m}
export LIMITS_MEMORY=${LIMITS_MEMORY:-128Mi}

cat *.k8s.yml | envsubst | kubectl apply -f -

if [ -n "$TELEGRAM_CHAT_ID" ]; then
    curl -X POST \
        -H 'Content-Type: application/json' \
        -d "{\"chat_id\": \"$TELEGRAM_CHAT_ID\", \"text\": \"\`$SERVICE\` has been deployed new version, version: \`$VERSION\`\", \"parse_mode\": \"markdown\"}" \
        https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage
fi
