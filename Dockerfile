FROM docker:19.03-git

ARG HELM_VERSION=3.2.1
ARG KUBECTL_VERSION=1.17.5

# Install helm (latest release)
# ENV BASE_URL="https://storage.googleapis.com/kubernetes-helm"
ENV BASE_URL="https://get.helm.sh"
ENV TAR_FILE="helm-v${HELM_VERSION}-linux-amd64.tar.gz"
RUN apk add --update --no-cache curl ca-certificates bash \
      jq gettext python3 \
    && curl -L ${BASE_URL}/${TAR_FILE} | tar xvz \
    && mv linux-amd64/helm /usr/bin/helm \
    && chmod +x /usr/bin/helm \
    && rm -rf linux-amd64 \
    && rm -f /var/cache/apk/* \
    && curl -LO https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
    && mv kubectl /usr/bin/kubectl \
    && chmod +x /usr/bin/kubectl \
    && python3 -m ensurepip \
    && pip3 install --upgrade pip \
    && pip3 install awscli pyyaml \
    && ln -sf $(which python3) $(dirname $(which python3))/python \
    && ln -sf $(which pip3) $(dirname $(which pip3))/pip

ADD ./script/deploy.sh /deploy.sh
