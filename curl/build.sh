#!/bin/bash

IMAGE="${REGISTRY_BASE_IMAGE}/curl:latest"

export DOCKER_BUILDKIT=1
docker build -t $IMAGE .
docker push $IMAGE
