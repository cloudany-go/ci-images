#!/bin/bash

IMAGE="${REGISTRY_BASE_IMAGE}/golang:1.15"

docker build -t $IMAGE .
docker push $IMAGE
