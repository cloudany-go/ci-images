#!/bin/bash

IMAGE="${REGISTRY_BASE_IMAGE}/alpine:${VERSION:-3.11}"

export DOCKER_BUILDKIT=1
docker build -t $IMAGE .
docker push $IMAGE
