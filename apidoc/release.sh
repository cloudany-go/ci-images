#!/bin/bash

# release the image
image="${CI_REGISTRY_IMAGE:-registry.gitlab.com/requestcloud/baseci}/apidoc:latest"

docker build -t $image .
docker push $image
