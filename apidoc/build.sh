#!/bin/sh

# replace the URL in swagger template by $CI_PAGES_URL/swagger.yml

set -e

if [ ! -d "public" ]; then
	mkdir public
fi

if [ -z "$SWAGGER_FILE" ]; then
	SWAGGER_FILE="./api/swagger.yml"
fi

if [ ! -f "$SWAGGER_FILE" ]; then
	echo "🐛 swagger file '$SWAGGER_FILE' is not found"
	exit 1
fi

if [ ! -f "$SWAGGER_TEMPLATE" ]; then
	SWAGGER_TEMPLATE="/template.html"
fi

if [ -z "$TITLE" ]; then
	TITLE="${SERVICE:-general} | api document"
fi

echo "✓ generate template "
sed -E "s~const url = .+$~const url = '$CI_PAGES_URL/swagger.yml';~" "$SWAGGER_TEMPLATE" > ./public/index.html

echo "✓ replace title to $TITLE "
sed -i -E "s~\{API Document\}~${TITLE}~" ./public/index.html
cp -v "$SWAGGER_FILE" ./public/swagger.yml
