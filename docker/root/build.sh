#!/bin/sh

# this script is used to build docker image each service
# it relies on some gitlab CI variables like $CI_REGISTRY_IMAGE, $CI_COMMIT_REF_NAME, $CI_JOB_TOKEN

export image="${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}"
echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
export build_arg="--build-arg VERSION=${CI_COMMIT_REF_NAME}"
docker build -t ${image} ${build_arg} .
docker push ${image}
