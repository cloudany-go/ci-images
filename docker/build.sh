#!/bin/bash

IMAGE="${REGISTRY_BASE_IMAGE}/docker:19.03-git"

docker build -t $IMAGE .
docker push $IMAGE
